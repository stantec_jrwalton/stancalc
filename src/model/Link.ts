// Link.ts

import { Node } from './Node';
import { Point } from './Point';

export class Link {

    private _inletOffset: number = 0;
    private _outletOffset: number = 0;

    constructor(private _inlet: Point, private _outlet: Point) { }

    public setInlet(point: Point) {
        this._inlet = point;
    }

    public setInletOffset(offset: number) {
        this._inletOffset = offset;
    }

    public setOutlet(point: Point) {
        this._outlet = point;
    }

    public setOutletOffset(offset: number) {
        this._outletOffset = offset;
    }

    public get length(): number {
        return Point.distance(this._inlet, this._outlet);
    }

    public get slope(): number {
        return (this._outlet.z - this._inlet.z) / Point.horizontalDistance(this._inlet, this._outlet);
    }
}
