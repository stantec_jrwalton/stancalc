// Point.ts

export interface IPoint {
    x: number;
    y: number;
    z: number;
}

export abstract class Point implements IPoint {

    public static default(): IPoint {
        return { x: 0, y: 0, z: 0 };
    }

    public static distance(pointA: IPoint, pointB: IPoint): number {
        return Math.sqrt((pointA.x - pointB.x) ** 2 + (pointA.y - pointB.y) ** 2 + (pointA.z - pointB.z) ** 2);
    }

    public static horizontalDistance(pointA: IPoint, pointB: IPoint): number {
        return Point.distance(
            {
                x: pointA.x,
                y: pointA.y,
                z: 0
            },
            {
                x: pointB.x,
                y: pointB.y,
                z: 0
            }
        );
    }

    public x: number;
    public y: number;
    public z: number;

    public distanceFrom(point: IPoint): number {
        return Point.distance(this, point);
    }

    public abstract clone(): this;

}
