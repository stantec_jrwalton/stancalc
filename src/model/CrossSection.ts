// CrossSection.ts

export abstract class CrossSection {

    public abstract getArea(depth: number): number;

    public abstract getWettedPerimeter(depth: number): number;

    public abstract getTopWidth(depth: number): number;

    public getHydraulicRadius(depth: number): number {
        return this.getArea(depth) / this.getWettedPerimeter(depth);
    }

}
