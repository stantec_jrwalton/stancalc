// Channel.ts

import { CrossSection } from './CrossSection';
import { Link } from './Link';
import { Material } from './Material';
import { Point } from './Point';

export class Channel extends Link {

    constructor(inlet: Point, outlet: Point, public xs: CrossSection, public material: Material) {
        super(inlet, outlet);
    }

}
