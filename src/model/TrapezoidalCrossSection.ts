// TrapezoidalCrossSection.ts

import { CrossSection } from './CrossSection';

export class TrapezoidalCrossSection extends CrossSection {

    constructor(private _width: number, private _sideSlopes: number) {
        super();
    }

    public getArea(depth: number): number {
        return this._width * depth + this._sideSlopes * depth ** 2;
    }

    public getWettedPerimeter(depth: number): number {
        return this._width + 2 * Math.sqrt(1 + this._sideSlopes ** 2) * depth;
    }

    public getTopWidth(depth: number): number {
        return this._width + 2 * this._sideSlopes * depth;
    }

}
