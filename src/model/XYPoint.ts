// XYPoint.ts

import { IPoint, Point } from './Point';

export class XYPoint extends Point {

    public x: number;
    public y: number;
    public z: number;

    constructor(x?: number, y?: number, z?: number)
    constructor(point?: Partial<IPoint>)
    constructor(pointOrX?: number | IPoint, y: number = 0, z: number = 0) {
        super();
        if (typeof pointOrX === 'object') {
            this.x = pointOrX.x || 0;
            this.y = pointOrX.y || 0;
            this.z = pointOrX.z || 0;
        } else {
            this.x = pointOrX || 0;
            this.y = y || 0;
            this.z = z;
        }
    }

    public clone(): this {
        return new XYPoint(this.x, this.y, this.z) as this;
    }
}
