// CircularCrossSection.ts

import { CrossSection } from './CrossSection';

export class CircularCrossSection extends CrossSection {

    constructor(private _diameter: number) {
        super();
    }

    public getArea(depth: number): number {
        return Math.PI * depth ** 2;
    }

    public getWettedPerimeter(depth: number): number {
        return Math.PI * depth;
    }

    public getTopWidth(depth: number): number {
        return depth;
    }

    public getHydrualicRadius(depth: number): number {
        return this._diameter / 4;
    }

}
