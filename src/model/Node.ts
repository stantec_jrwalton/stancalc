// Node.ts

import { Link } from './Link';
import { IPoint, Point } from './Point';

export class Node implements IPoint {

    private readonly _point: Point;
    private readonly _offset: number;

    constructor(point: Point, offset: number) {
        this._point = point;
        this._offset = offset;
    }

    public get x(): number {
        return this._point.x;
    }

    public get y(): number {
        return this._point.y;
    }

    public get z(): number {
        return this._point.z;
    }

    public clone(): this {
        throw new Error('Method not implemented.');
    }
}
