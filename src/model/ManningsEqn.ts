// ManningsEqn.ts

import { findRoot } from '../util';
import { Channel } from './Channel';
import { CrossSection } from './CrossSection';

export function getFlow(channel: Channel, depth: number): number {
    return 1.49 / channel.material.roughness * channel.xs.getArea(depth) *
        Math.pow(channel.xs.getHydraulicRadius(depth), 2 / 3) * Math.sqrt(channel.slope);
}

export function getDepth(channel: Channel, flow: number): number {
    return findRoot((depth: number)=> flow - getFlow(channel, depth));
}
