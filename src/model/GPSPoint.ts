// GPSPoint.ts

import { Point } from './Point';

export class GPSPoint extends Point {

    constructor(private _latitude: number, private _longitude: number, public z: number = 0) {
        super();
    }

    public get x(): number {
        return this._longitude;
    }

    public get y(): number {
        return this._latitude;
    }

    public clone(): this {
        return new GPSPoint(this._latitude, this._longitude, this.z) as this;
    }

}
