
export function findRoot(fn: (x: number) => number, range: number[] = [0, 100], accuracy: number = 0.001, maxIterations = 50): number {
    let xa = range[0];
    let ya = fn(xa);
    let xb = range[1];
    let yb = fn(xb);
    let xc: number;
    let yc: number;
    do {
        xc = (xa + xb) / 2;
        yc = fn(xc);
        if (ya * yc > 0) {
            xa = xc;
            ya = yc;
        } else {
            xb = xc;
            yb = yc;
        }
    } while (xb - xa > accuracy);
    return xc;
}
